//https://imasters.com.br/front-end/criando-uma-api-node-em-10-passos-com-express-js 
//referencia


const express = require('express');
const app = express();
const router = express.Router();

const index = require('./routes/index');
const personRoute = require('./routes/personRoute');

app.use('/', index);
app.use('/persons', personRoute);
module.exports = app;