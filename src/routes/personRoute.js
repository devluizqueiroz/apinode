const express = require('express');
const router = express.Router();
////////////////bloc person///////////////////////////////////
const controller = require('../controllers/personController');
router.post('/', controller.post);
router.put('/:id', controller.put);
router.delete('/:id', controller.delet);
///////////////end block/////////////////////////////////////
module.exports = router;
